import React from "react";
import { BrowserRouter, NavLink, Route, Routes } from "react-router-dom";
import { QueryClientProvider, QueryClient } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";

import {
  DependentQueries,
  DynamicParallel,
  Hello,
  Home,
  ParallelQueries,
  RQSuperHero,
  RQSuperHeroes,
  SuperHeroes,
} from "./pages";
import "./app.css";

const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <BrowserRouter>
        <div>
          <nav>
            <ul>
              <li>
                <NavLink to="/">Home</NavLink>
              </li>
              <li>
                <NavLink to="/super-heroes">Traditional Super Heroes</NavLink>
              </li>
              <li>
                <NavLink to="/rq-super-heroes">RQ Super Heroes</NavLink>
              </li>
              <li>
                <NavLink to="/hello">Hello</NavLink>
              </li>
              <li>
                <NavLink to="/rq-dynamic-parallel">
                  Dynamic Parallel Queries
                </NavLink>
              </li>
              <li>
                <NavLink to="/rq-dependent-queries">Dependent Queries</NavLink>
              </li>
            </ul>
          </nav>
          <Routes>
            <Route index element={<Home />} />
            <Route path="super-heroes" element={<SuperHeroes />} />
            <Route path="rq-super-heroes/:heroId" element={<RQSuperHero />} />
            <Route path="rq-super-heroes" element={<RQSuperHeroes />} />
            <Route path="hello" element={<Hello />} />
            <Route path="rq-parallel" element={<ParallelQueries />} />
            <Route
              path="rq-dynamic-parallel"
              element={<DynamicParallel heroIds={[1, 3]} />}
            />
            <Route
              path="rq-dependent-queries"
              element={<DependentQueries email="dharmjeet@gmail.com" />}
            />
          </Routes>
        </div>
      </BrowserRouter>
      <ReactQueryDevtools initialIsOpen={false} position="bottom-right" />
    </QueryClientProvider>
  );
}

export default App;
