import axios from "axios";
import { useQuery } from "react-query";

const fetchSuperHero = ({ queryKey }) => {
  const heroId = queryKey[1];
  return axios.get(`http://localhost:4000/superheroes/${heroId}`);
};

export const useSuperHeroeData = (heroId) => {
  return useQuery(["super-hero", heroId], fetchSuperHero, {
    // cacheTime: 5000,                      // default: 5 minute
    // staleTime: 30000,                     // default: 0
    // refetchOnMount: true,                 // default: true
    // refetchOnWindowFocus: false,
    // refetchInterval: 3000,                // default: false
    // refetchIntervalInBackground: true,
    // enabled,
    // onSuccess,
    // onError,
    // select: (data) => {
    //   const superHeroNames = data?.data.map((hero) => hero.name);
    //   return superHeroNames;
    // },
  });
};
