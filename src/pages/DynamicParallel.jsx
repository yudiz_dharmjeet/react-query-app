import React from "react";
import axios from "axios";
import { useQueries } from "react-query";
import PropTypes from "prop-types";

const fetchSuperHero = (heroId) => {
  return axios.get(`http://localhost:4000/superheroes/${heroId}`);
};

function DynamicParallel({ heroIds }) {
  const queryResults = useQueries(
    heroIds.map((id) => {
      return {
        queryKey: ["super-hero", id],
        queryFn: () => fetchSuperHero(id),
      };
    })
  );

  console.log({ queryResults });

  return <div>DynamicParallel</div>;
}

DynamicParallel.propTypes = {
  heroIds: PropTypes.array,
};

export default DynamicParallel;
