import Home from "./Home";
import SuperHeroes from "./SuperHeroes";
import RQSuperHeroes from "./RQSuperHeroes";
import Hello from "./Hello";
import RQSuperHero from "./RQSuperHero";
import ParallelQueries from "./ParallelQueries";
import DynamicParallel from "./DynamicParallel";
import DependentQueries from "./DependentQueries";

export {
  Home,
  SuperHeroes,
  RQSuperHeroes,
  Hello,
  RQSuperHero,
  ParallelQueries,
  DynamicParallel,
  DependentQueries,
};
