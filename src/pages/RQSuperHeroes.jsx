import React from "react";
import { Link } from "react-router-dom";

import { useSuperHeroesData } from "../hooks/useSuperHeroesData";

function RQSuperHeroes() {
  const onSuccess = (data) => {
    console.log(data);
  };

  const onError = (error) => {
    console.log("Perform side effect after encountering error", error);
  };

  const { isLoading, isFetching, data, isError, error } = useSuperHeroesData(
    onSuccess,
    onError,
    true
  );

  if (isLoading || isFetching) {
    return <h2>Loading...</h2>;
  }

  if (isError) {
    return <h2>{error.message}</h2>;
  }

  return (
    <>
      <h2>RQ Super Heroes</h2>
      {data?.data.map((hero) => {
        return (
          <div key={hero.id}>
            <Link to={`/rq-super-heroes/${hero.id}`}>{hero.name}</Link>
          </div>
        );
      })}
      {/* {data?.map((heroName) => {
        return <div key={heroName}>{heroName}</div>;
      })} */}
    </>
  );
}

export default RQSuperHeroes;
