import React from "react";
import { useQuery } from "react-query";
import axios from "axios";
import PropTypes from "prop-types";

const fetchUserByEmail = (email) => {
  return axios.get(`http://localhost:4000/users/${email}`);
};

const fetchCoursesByChannelId = (channelID) => {
  return axios.get(`http://localhost:4000/channels/${channelID}`);
};

function DependentQueries({ email }) {
  const { data: user } = useQuery(["user", email], () =>
    fetchUserByEmail(email)
  );
  const channelID = user?.data.channelId;

  const { data: courses } = useQuery(
    ["courses", channelID],
    () => fetchCoursesByChannelId(channelID),
    {
      enabled: !!channelID,
    }
  );

  console.log(courses?.data.courses);

  return (
    <div>
      {courses?.data.courses.map((course) => (
        <h2 key={course}>{course}</h2>
      ))}
    </div>
  );
}

DependentQueries.propTypes = {
  email: PropTypes.string,
};

export default DependentQueries;
